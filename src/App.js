import logo from './logo.svg';
import './App.css';
import { useState, createContext } from 'react';
import Experience from './Experience';
import Education from './Education';
import Skills from './Skills';
import { Button } from 'react-bootstrap';
import { UserContext } from './UserContext';
import Builder from './Builder';


function App() {

  const [state, setState] = useState({
    name: "Enter Your Name",
    email: "Enter Your Email",
    address: "Enter Your Address",
    phone: "Enter Your Phone",
    experiences: [{
      id: 0,
      company: 'Enter Company',
      year: 'Enter Year',
      designation: 'Enter Designation'
    }],
    education: [{
      id: 0,
      institute: 'Enter Institute',
      year: 'Enter Year',
      degree: 'Enter Degree'
    }],
    skills: [
    ]
  })

  const dummyExperience = {
    id: state.experiences[state.experiences.length - 1].id + 1,
    company: 'Enter Company',
    year: 'Enter Year',
    designation: 'Enter Designation'
  }

  const dummyEducationData = {
    id: state.education[state.education.length - 1].id + 1,
    institute: 'Enter Institute',
    year: 'Enter Year',
    degree: 'Enter Degree'
  }

  const handleChangeExperience = (e, index) => {
    const { name, value } = e.target
    let experiences = [...state.experiences]
    experiences[index] = { ...experiences[index], [name]: value }
    setState({ ...state, experiences })
  }

  const handleRemoveExperience = (index) => {

    let experiences = state.experiences.filter(e => e.id !== index)
    setState({ ...state, experiences })
  }

  const handleAddExperience = () => {
    let experiences = [...state.experiences, dummyExperience]
    setState({ ...state, experiences })
  }

  const handleChangeEducation = (e, index) => {
    const { name, value } = e.target
    let education = [...state.education]
    education[index] = { ...education[index], [name]: value }
    setState({ ...state, education })
  }

  const handleRemoveEducation = (index) => {

    let education = state.education.filter(e => e.id !== index)
    setState({ ...state, education })
  }

  const handleAddEducation = () => {
    let education = [...state.education, dummyEducationData]
    setState({ ...state, education })
  }

  const handleChange = (e) => {
    setState({ ...state, [e.target.name]: e.target.value })
  }

  const onDelete = (i) => {
    const tags = state.skills.slice(0)
    tags.splice(i, 1)
    setState({ ...state, skills : tags })
}

const onAddition = (tag) => {
    const tags = [].concat(state.skills, tag)
    setState({...state, skills :tags })
}


  return (

    <UserContext.Provider value={[state, setState]}>

      <div className="App">
        <h2>Lets Build Our Resume</h2>

        <input class="form-control" type="text" name="name" placeholder={state.name} onChange={handleChange} />
        <input class="form-control" type="email" name="email" placeholder={state.email} onChange={handleChange} />
        <textarea class="form-control" type="text" name="address" placeholder={state.address} onChange={handleChange} />
        <input class="form-control" type="number" name="phone" placeholder={state.phone} onChange={handleChange} />


        <Experience values={state.experiences} onChange={handleChangeExperience} onAdd={handleAddExperience} onRemove={handleRemoveExperience} />

        <Education values={state.education} onChange={handleChangeEducation} onAdd={handleAddEducation} onRemove={handleRemoveEducation} />

        <Skills values={state} onDelete={onDelete} onAddition={onAddition}/>


        


      </div>

      <Builder />
    </UserContext.Provider>
  );
}

export default App;

