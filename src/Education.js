import React, { useState } from 'react';

function Education(props) {
    const {values, onChange, onAdd, onRemove } = props

    return (
        <div className="rel">
            <h3 className="align:center">Education</h3>

            
            <button type="button" class="btn-sm btn-primary button-align" onClick={onAdd}>+</button>
            { values.map((data, index) =>
                <div key={data.id} className="rel">
                    Education #{index + 1}
                    <input class="form-control" type="text" name="institute" placeholder={data.institute} onChange={(e) => onChange(e, data.id)} />
                    <input class="form-control" type="email" name="year" placeholder={data.year} onChange={(e) => onChange(e, data.id)} />
                    <input class="form-control" type="text" name="degree" placeholder={data.degree} onChange={(e) => onChange(e, data.id)} />
                    {values.length > 1 ? <button type="button" class="btn-sm btn-danger button-align" onClick={() => onRemove(data.id)}>X</button> : ""}

                </div>

            )}

        </div>
    );
}

export default Education;