import React, { useContext, useState } from 'react';
import ReactTags from 'react-tag-autocomplete'
import { UserContext } from './UserContext';

function Skills(props) {

    const [state, setState] = useContext(UserContext)
    
    const {onAddition, onDelete} = props

    const [skills, setSkills] = useState({
        suggestions: [
            { id: 3, name: "Php" },
            { id: 4, name: "Javascript" },
            { id: 5, name: "ReactJs" },
            { id: 6, name: "NodeJS" }
        ]
    })

    const reactTags = React.createRef()

    
    return (
        <div>
            <h3>Skills</h3>
            <ReactTags
                ref={reactTags}
                tags={state.skills}
                suggestions={skills.suggestions}
                onDelete={onDelete}
                onAddition={onAddition} />
        </div>
    );
}

export default Skills;