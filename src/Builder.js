import React, { useContext } from 'react';
import { UserContext } from './UserContext';

function Builder(props) {
    const [state, setState] = useContext(UserContext);


    return (
        <div className="generated-resume">

            <h1>Generated Resume</h1>

            <header>
                <h3 className="name">{state.name}</h3>
                <div className="address">{state.address} </div>
                <div className="">{state.email} , {state.phone}</div>
            </header>

            <content>
                <div className="experience">
                    <h4>Experience</h4>
                    {state.experiences.map((data, index) =>
                        <div className="experience-details">
                            <div>{data.designation}</div>
                            <div>{data.company}</div>
                            <div>{data.year}</div>
                        </div>)}
                </div>

                <div className="education">
                    <h4>Education</h4>
                    {state.education.map((data, index) =>

                        <div className="experience-details">
                            <div> {data.institute}</div>
                            <div>{data.degree}</div>
                            <div>{data.year}</div>
                        </div>
                    )}
                </div>

            </content>

            <div className="skills">
                <h4>Skills</h4>
                {state.skills.map(skill =>
                    <div className="skill">
                        {skill.name}, 
                    </div>
                )}

            </div>

        </div>
    );
}

export default Builder;