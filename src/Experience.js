import React, { useState, useContext } from 'react';
import { UserContext } from './UserContext';

function Experience(props) {

    const state = useContext(UserContext);

    const {values, onChange, onAdd, onRemove } = props

    return (
        <div className="rel">
            <h3>Experience</h3>

            <button type="button" class="btn-sm btn-primary button-align" onClick={onAdd}>+</button>
            { values.map((data, index) =>
                <div key={data.id} className="rel">
                    Experience #{index + 1}
                    <input class="form-control" type="text" name="company" placeholder={data.company} onChange={(e) => onChange(e, data.id)} />
                    <input class="form-control"type="email" name="year" placeholder={data.year} onChange={(e) => onChange(e, data.id)} />
                    <input class="form-control" type="text" name="designation" placeholder={data.designation} onChange={(e) => onChange(e, data.id)} />
                    {values.length > 1 ? <button type="button" class="btn-sm btn-danger button-align" onClick={() => onRemove(data.id)}>X</button> : ""}

                </div>

            )}

        </div>
    );
}

export default Experience;